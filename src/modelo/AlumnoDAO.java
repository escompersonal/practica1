/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AlumnoDAO {
    public static final String SQL_INSERT = "INSERT INTO "
            + "Alumno (?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?)";
    public static final String SQL_DELETE = "DELETE FROM "
            + "Alumno WHERE idAlumno = ?";
    public static final String SQL_UPDATE = "UPDATE Alumno SET "
            + "horario = ?, "
            + "tipoCurso= ? "
            + "WHERE idAlumno = ¡";
    public static final String SQL_SELECT = "SELECT * FROM Alumno "
            + "WHERE idAlumno = ?";
    public static final String SQL_SELECT_ALL = "SELECT * FROM Alumno";


    public boolean create(Alumno dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(SQL_INSERT);
            ps.setInt(1, dto.getIdAlumno());
            ps.setString(2, dto.getNombre());
            ps.setString(3, dto.getPaterno());
            ps.setString(4, dto.getMaterno());
            ps.setString(5, dto.getEmail());
            ps.setString(6, dto.getCalle());
            ps.setString(7, dto.getNumero());
            ps.setString(8, dto.getColonia());
            ps.setString(9, dto.getDelegacion());
            ps.setString(10, dto.getNombre());
            ps.setString(11, dto.getNombre());
            return !(0 == ps.executeUpdate());
        } finally {
            cerrar(ps);
            cerrar(conn);
        }
    }

    public boolean delete(Alumno dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(SQL_DELETE);
            ps.setInt(1, dto.getIdAlumno());
            return !(0 == ps.executeUpdate());
        } finally {
            cerrar(ps);
            cerrar(conn);
        }
    }

    public boolean update(Alumno dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(SQL_UPDATE);
            ps.setString(1, dto.getNombre());
            ps.setInt(2, dto.getIdAlumno());
            return !(0 == ps.executeUpdate());
        } finally {
            cerrar(ps);
            cerrar(conn);
        }
    }

    public Alumno load(Alumno dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(SQL_SELECT);
            ps.setInt(1, dto.getIdAlumno());
            rs = ps.executeQuery();
            List results = getResults(rs);
            if (results.size() > 0) {
                return (Alumno) results.get(0);
            } else {
                return null;
            }
        } finally {
            cerrar(rs);
            cerrar(ps);
            cerrar(conn);
        }
    }

    public List loadAll(Connection conn) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(SQL_SELECT_ALL);
            rs = ps.executeQuery();
            List results = getResults(rs);
            if (results.size() > 0) {
                return results;
            } else {
                return null;
            }
        } finally {
            cerrar(rs);
            cerrar(ps);
            cerrar(conn);
        }
    }

    private List getResults(ResultSet rs) throws SQLException {
        List results = new ArrayList();
        while (rs.next()) {
            Alumno dto = new Alumno();
            dto.setIdAlumno(rs.getInt("id_categoria"));
            dto.setNombre(rs.getString("nomb"));
            results.add(dto);
        }
        return results;
    }

    private void cerrar(PreparedStatement ps) throws SQLException {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
    }

    private void cerrar(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
            }
        }
    }

    private void cerrar(Connection cnn) {
        if (cnn != null) {
            try {
                cnn.close();
            } catch (SQLException e) {
            }
        }
    }

}
