/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AsignacionDAO {
    public static final String SQL_INSERT = "INSERT INTO "
            + "Asignacion (?, ?, ?, ?)";
    public static final String SQL_DELETE = "DELETE FROM "
            + "Asignacion WHERE idAlumno = ? AND idCurso = ?";
    public static final String SQL_UPDATE = "UPDATE Asignacion SET "
            + "horario = ?, "
            + "tipoCurso= ? "
            + "WHERE idAlumno = ? AND idCurso = ?";
    public static final String SQL_SELECT = "SELECT * FROM Asignacion "
            + "WHERE idAlumno = ? AND idCurso = ?";
    public static final String SQL_SELECT_ALL = "SELECT * FROM Asignacion";

    public boolean create(Asignacion dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(SQL_INSERT);
            ps.setInt(1, dto.getIdAlumno());
            ps.setInt(2, dto.getIdCurso());
            ps.setString(3, dto.getHorario());
            ps.setInt(4, dto.getTipoCurso());            
            return !(0 == ps.executeUpdate());
        } finally {
            cerrar(ps);
            cerrar(conn);
        }
    }

    public boolean delete(Asignacion dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(SQL_DELETE);
            ps.setInt(1, dto.getIdAlumno());
            ps.setInt(2, dto.getIdCurso());
            return !(0 == ps.executeUpdate());
        } finally {
            cerrar(ps);
            cerrar(conn);
        }
    }

    public boolean update(Asignacion dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(SQL_UPDATE);
            ps.setInt(1, dto.getIdAlumno());
            ps.setInt(2, dto.getIdCurso());
            return !(0 == ps.executeUpdate());
        } finally {
            cerrar(ps);
            cerrar(conn);
        }
    }

    public Asignacion load(Asignacion dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(SQL_SELECT);
            ps.setInt(1, dto.getIdAlumno());
            ps.setInt(2, dto.getIdCurso());
            rs = ps.executeQuery();
            List results = getResults(rs);
            if (results.size() > 0) {
                return (Asignacion) results.get(0);
            } else {
                return null;
            }
        } finally {
            cerrar(rs);
            cerrar(ps);
            cerrar(conn);
        }
    }

    public List loadAll(Connection conn) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(SQL_SELECT_ALL);
            rs = ps.executeQuery();
            List results = getResults(rs);
            if (results.size() > 0) {
                return results;
            } else {
                return null;
            }
        } finally {
            cerrar(rs);
            cerrar(ps);
            cerrar(conn);
        }
    }

    private List getResults(ResultSet rs) throws SQLException {
        List results = new ArrayList();
        while (rs.next()) {
            Asignacion dto = new Asignacion();
            dto.setIdCurso(rs.getInt("id_categoria"));
            dto.setIdAlumno(rs.getInt("nomb"));
            dto.setHorario(rs.getString("nomb"));
            dto.setTipoCurso(rs.getInt("nomb"));
            results.add(dto);
        }
        return results;
    }

    private void cerrar(PreparedStatement ps) throws SQLException {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
    }

    private void cerrar(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
            }
        }
    }

    private void cerrar(Connection cnn) {
        if (cnn != null) {
            try {
                cnn.close();
            } catch (SQLException e) {
            }
        }
    }

}
