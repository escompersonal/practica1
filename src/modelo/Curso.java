/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Date;

/**
 *
 * @author alumno
 */
public class Curso {
    private int idCurso;
    private String nombre;
    private Date fechaInicio;
    private Date fechaTermino;
    private float cuotaDeRecuperacion;

    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(Date fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public float getCuotaDeRecuperacion() {
        return cuotaDeRecuperacion;
    }

    public void setCuotaDeRecuperacion(float cuotaDeRecuperacion) {
        this.cuotaDeRecuperacion = cuotaDeRecuperacion;
    }
    
    
}
