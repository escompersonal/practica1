/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Curso;

public class CursoDAO {

    public static final String SQL_INSERT = "INSERT INTO "
            + "Curso (?, ?, ?, ?, ?)";
    public static final String SQL_DELETE = "DELETE FROM "
            + "Curso WHERE idCurso = ?";
    public static final String SQL_UPDATE = "UPDATE Curso SET "
            + "nombre = ?, "
            + "fechaInicio = ?, "
            + "fechaTermino = ?, "
            + "cuotaDeRecuparacion = ? "
            + "WHERE idCurso = ? ";
    public static final String SQL_SELECT = "SELECT * FROM Curso "
            + "WHERE idCurso = ?";
    public static final String SQL_SELECT_ALL = "SELECT * FROM Curso";

    public boolean create(Curso dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(SQL_INSERT);
            ps.setInt(1, dto.getIdCurso());
            ps.setString(2, dto.getNombre());
            ps.setDate(3, (Date) dto.getFechaInicio());
            ps.setDate(4, (Date) dto.getFechaTermino());
            ps.setFloat(5, dto.getCuotaDeRecuperacion());
            return !(0 == ps.executeUpdate());
        } finally {
            cerrar(ps);
            cerrar(conn);
        }
    }

    public boolean delete(Curso dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(SQL_DELETE);
            ps.setInt(1, dto.getIdCurso());
            return !(0 == ps.executeUpdate());
        } finally {
            cerrar(ps);
            cerrar(conn);
        }
    }

    public boolean update(Curso dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(SQL_UPDATE);
            ps.setInt(1, dto.getIdCurso());
            ps.setString(2, dto.getNombre());
            ps.setDate(3, (Date) dto.getFechaInicio());
            ps.setDate(4, (Date) dto.getFechaTermino());
            ps.setFloat(5, dto.getCuotaDeRecuperacion());
            return !(0 == ps.executeUpdate());
        } finally {
            cerrar(ps);
            cerrar(conn);
        }
    }

    public Curso load(Curso dto, Connection conn) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(SQL_SELECT);
            ps.setInt(1, dto.getIdCurso());
            rs = ps.executeQuery();
            List results = getResults(rs);
            if (results.size() > 0) {
                return (Curso) results.get(0);
            } else {
                return null;
            }
        } finally {
            cerrar(rs);
            cerrar(ps);
            cerrar(conn);
        }
    }

    public List loadAll(Connection conn) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(SQL_SELECT_ALL);
            rs = ps.executeQuery();
            List results = getResults(rs);
            if (results.size() > 0) {
                return results;
            } else {
                return null;
            }
        } finally {
            cerrar(rs);
            cerrar(ps);
            cerrar(conn);
        }
    }

    private List getResults(ResultSet rs) throws SQLException {
        List results = new ArrayList();
        while (rs.next()) {
            Curso dto = new Curso();
            dto.setIdCurso(rs.getInt("idCurso"));
            dto.setNombre(rs.getString("nombre"));
            dto.setFechaInicio(rs.getDate("fechaInicio"));
            dto.setFechaTermino(rs.getDate("fechaTermino"));
            dto.setCuotaDeRecuperacion(rs.getFloat("coutaRecuperacion"));
            results.add(dto);
        }
        return results;
    }

    private void cerrar(PreparedStatement ps) throws SQLException {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
            }
        }
    }

    private void cerrar(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
            }
        }
    }

    private void cerrar(Connection cnn) {
        if (cnn != null) {
            try {
                cnn.close();
            } catch (SQLException e) {
            }
        }
    }

}
