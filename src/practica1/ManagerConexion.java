/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1;

import modelo.CursoDAO;
import modelo.AlumnoDAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import modelo.Alumno;
import modelo.Asignacion;
import modelo.AsignacionDAO;
import modelo.Curso;

/**
 *
 * @author alumno
 */
public class ManagerConexion {

    public static final int LIST_ALUMNO = 0;
    public static final int LIST_CURSO = 1;
    public static final int LIST_ASIGNACION = 2;

    private Connection cnn;
    private AlumnoDAO aDao;
    private CursoDAO cDao;
    private AsignacionDAO asDao;

    public ManagerConexion() {
        String user = "root";
        String pwd = "root";
        String url = "jdbc:mysql://localhost:3306/Distribuidos";
        String mySqlDriver = "com.mysql.jdbc.Driver";
        aDao = new AlumnoDAO();
        try {
            Class.forName(mySqlDriver);
            cnn = DriverManager.getConnection(url, user, pwd);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }

    public boolean alta(Alumno a) throws SQLException {
        return aDao.create(a, cnn);
    }

    public boolean baja(Alumno a) throws SQLException {
        return aDao.delete(a, cnn);
    }

    public boolean editar(Alumno a) throws SQLException {
        return aDao.update(a, cnn);
    }

    public Alumno buscar(Alumno a) throws SQLException {
        return aDao.load(a, cnn);
    }

    public List listar(int opc) throws SQLException {
        switch(opc){
            case LIST_ALUMNO:
                return aDao.loadAll(cnn);
            case LIST_CURSO:
                return cDao.loadAll(cnn);
            case LIST_ASIGNACION:
                return asDao.loadAll(cnn);
            default:
                return null;
        }        
    }
    
//------------------------------------------------------------------------------//    
    
    
    public boolean alta(Curso c) throws SQLException {
        return cDao.create(c, cnn);
    }

    public boolean baja(Curso c) throws SQLException {
        return cDao.delete(c, cnn);
    }

    public boolean editar(Curso c) throws SQLException {
        return cDao.update(c, cnn);
    }

    public Curso buscar(Curso c) throws SQLException {
        return cDao.load(c, cnn);
    }
    
//------------------------------------------------------------------------------//    
    
    
    public boolean alta(Asignacion a) throws SQLException {
        return asDao.create(a, cnn);
    }

    public boolean baja(Asignacion a) throws SQLException {
        return asDao.delete(a, cnn);
    }

    public boolean editar(Asignacion a) throws SQLException {
        return asDao.update(a, cnn);
    }

    public Asignacion buscar(Asignacion a) throws SQLException {
        return asDao.load(a, cnn);
    }

   

}
